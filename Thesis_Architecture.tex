%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Architecture.tex                                    %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Valter Mário                                             %
%     Last modified :  27 December 2018                                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{The Previous Versat Architecture}
\label{chapter:architecture}

This chapter describes the Versat architecture as it was found when this work
started. The Versat CGRA architecture is essentially a full mesh of functional
units (FUs), including memories, called the Data Engine (DE). The configurations
of the DE, i.e., the configurations of the FUs and interconnections between them
are stored and managed in the Configuration Module. The control part consists of
a 16-Instruction Controller, a Program Memory, a Control Register File (CRF) and a
Direct Memory Access (DMA) module. The block diagram is presented in
Figure~\ref{fig_top}.

\begin{figure}[!htb]
\centering \includegraphics[width=0.75\textwidth]{drawings/top.pdf}
\caption{Versat Architecture.~\cite{lopestese}}
\label{fig_top}
\end{figure}

The "brain" of this CGRA is the controller, which executes the programs stored in
the Program Memory (9 kB = 1 kB boot ROM + 8 kB RAM). The programs written by
the user are loaded to the RAM. 

A typical program needs to configure DE multiple times and manage the DMA data
transfers. The Controller writes DE configurations to the Configuration Module
and can load/save data to/from the DE using the DMA engine. The DMA module can
also be used to initially load the Versat program or to move CGRA configurations
between the core and the external memory.

The Versat core has 2 different interfaces, one connected to CRF that is used
for the host to communicate with Versat, called the host interface, and another
connected to DMA to allow data transfers with the external memory, called the
memory interface. Both interfaces use ARM's Advanced eXtensible Interface (AXI),
which derives from the Advanced Microcontroller Bus Architecture (AMBA).

In the next sections the DE and Controller blocks will be described in detail,
as these are the most relevant blocks for the prosecution of this work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Data Engine}
\label{section:dataEngine}
The Data Engine (DE) has a fixed topology that consists in 15 Functional Units
(FUs) as shown in Figure~\ref{fig_de}.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.6\textwidth]{drawings/de.pdf}
\caption{Data Engine.}
\label{fig_de}
\end{figure}

The DE has a 32-bit architecture and is composed of the following reconfigurable
FUs: 6 Arithmetic and Logic Units (ALUs), 4 multipliers, 1 barrel shifter and 4
dual-port embedded memories ($8kB$ each). The memories are also treated as FUs
for simplicity.  As can be seen in Figure~\ref{fig_de}, the FUs are
interconnected by a Data Bus which is the concatenation of all FU outputs. Being
a 32-bit architecture, each output contributes one 32-bit section to the Data
Bus except the dual-port memories which contribute two 32-bit sections. Being a
full mesh topology, every FU input can select any Data Bus section, depending on
its configuration. The configurations are placed in registers in the
Configuration Module, whose outputs are also concatenated to build a bus called
the Config Bus. Each FU input can ignore the input value and simply store some
value in its output register to be used as a constant in the datapath, working
as a register shared with the Controller.

In Figure~\ref{fig_fu}, it is shown in detail how a particular FU, in this
example an ALU, is connected to the control, data and configuration buses.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.7\textwidth]{drawings/FU.pdf}
\caption{Detail Connections of an ALU.~\cite{lopestese}}
\label{fig_fu}
\end{figure}

Each FU input has a programmable multiplexer to select one of the sections of
the Data Bus. In Figure~\ref{fig_fu} the Config Bus is connected to all FUs but,
in fact, only the configuration bits of each FU are connected to it. These bits
are called the {\em configuration space} of the FU. The configuration space is
further divided in several {\em configuration fields}, which are 3 in the ALU
example: the selection of input A (5 bits), the selection of input B (5 bits)
and the selection of the function (4 bits). The partial reconfiguration scheme
works at the field level, so it is only possible to reconfigure these fields
individually~\cite {lopestese}. Since each FU has its own {\em configuration
  space}, each configuration of the DE can implement more than one datapath.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dual-Port Memories}
\label{subsection:addressGenerationUnit}

Each dual-port RAM memory has two data inputs, two data outputs and two Address
Generation Units (AGUs). An important feature of Versat is the fact that the
AGUs are hardwired in each memory port, so that it is not need to place and
route these components. The AGU has 2 cascaded counters which allows Versat to
execute two nested loops in one configuration. The AGU can be programmed to
generate the address sequence for accessing data from the memory port, during
the execution of a program loop. The Versat AGU scheme is similar to the one
described in~\cite{Farahini14}, because both schemes use parallel and
distributed AGUs. They can start execution with a programmable delay and each
one can be operated independently, so that circuit paths with different
latencies can be synchronized.

The AGU parameters are described in Table \ref{tab:MemParameter}. The memory
read latency is 1 clock cycle. The latency of the other FUs are given in their
respective sections.

\begin{table}[h!]
    \caption{Address Generator Unit Parameters.~\cite{rec17}}
  \begin{center}
	  \begin{tabular}{|m{2 cm}|m{10 cm}|}
      \hline
      {\bf Parameter} & {\bf Description} \\
      \hline \hline
      Start & Memory start address. Default value is 0. \\
      \cline{1-2}
      Per  & Number of iterations of the inner loop, aka Period. Default is 1 (no inner loop). \\
      \cline{1-2}
      Duty & Number of cycles in a period (Per) that the memory is enabled. Default is 1.\\
      \cline{1-2}
      Incr & Increment for the inner loop. Default is 0.\\
      \cline{1-2}
      Iter & Number of iterations of the outer loop. Default is 1.\\
      \cline{1-2}
      Shift & Additional increment in the end of each period. Note that Per+Shift is the increment of the outer loop. Default is 0.\\
      \cline{1-2}
      Delay & Number of clock cycles that the address generator must wait before starting to work. Used to compensate different latencies in the converging branches of the configured datapaths. Default is 0.\\
      \cline{1-2}
      Reverse & Bit-wise reversion of the generated address. Default is 0.\\
      \hline
    \end{tabular}
  \end{center}
  \label{tab:MemParameter}
\end{table}

The configuration of the input multiplexer present at each memory port
determines what the port does. There are the following options:
\begin{itemize}
	\item READ: the port reads the memory; the address comes
	      from the AGU or from the other input port of the memory (external pointer).
	\item WRITE: the port writes the memory; the memory is also
		read at the same address before it is written; the Data
		Bus section to be written must be selected; the address
		comes from the AGU or external pointer.
	\item DGU (Data Generation Unit): the sequence generated
		by the AGU is output directly instead of being used as the
		address. A DGU can be used for generating data patterns,
		synchronization and control signals for the FUs.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{ALU Functions}
\label{subsection:arithmeticLogicUnit}

The ALU functions are given in Table \ref{tab:AluOpers}, A and B are the inputs
and Y is the output. The DE has two different types of ALUs: Type I and Type
II. There are two ALUs of Type 1 and four ALUs of Type 2. They are similar, having
the same latency, 2 clock cycles. The main difference between them is that Type
2 uses one of the four configuration bits to create an internal feedback loop
from the output to one of the inputs, which is useful in many applications.

\begin{table}[!htb]
  \caption{ALU Functions.}
  \centering
    \begin{tabular}{|l|l|l|}
      \hline
      {\bf Operation} & {\bf Type I} & {\bf Type II} (w/ feedback)\\
    \hline \hline 
     Logic OR & Y = A $|$ B & Y = Y $|$ B \\
    \hline
     Logic AND & Y = A \& B  & Y = Y \& B \\
    \hline
     Logic XOR & Y = A $\oplus$ B  & Y = Y $\oplus$ B\\
    \hline
     Addition & Y = A + B  & Y = A$\geq$0? Y+B: B\\
    \hline
     Multiplexer & Y = A$\geq$0? B: 0 & Y = A$\geq$0? Y: B\\
    \hline
     Subtraction & Y = B - A  & Y = A$\geq$0? Y-B: B\\
    \hline
     Sign extend 8 & Y = A[7]...A[7..0]  & NA\\
    \hline
     Sign extend 16 & Y = A[15]...A[15..0]  & NA\\
    \hline
     Shift right arithmetic &  Y = {A[31], A[31..1]} & NA\\
    \hline
     Shift right logical & Y = {'0', A[31..1]}  & NA\\
    \hline
     Signed compare & Y[31] = (A$>$B) &  Y[31] = Y$>$B \\
    \hline
     Unsigned compare & Y[31] = (A$>$B) & Y[31] = Y$>$B \\
    \hline
     Count lead zeros & Y = CLZ(A) & NA \\
    \hline
     Signed maximum & Y=max(A,B) & Y = A$\geq$0? max(Y,B): Y\\
    \hline
     Signed minimum & Y=min(A,B) & Y = A$\geq$0? min(Y,B): Y\\
    \hline
     Absolute value & Y=$|$A$|$  & NA\\
    \hline     
    \end{tabular}
  \label{tab:AluOpers}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Multipliers and Barrel Shifter}
\label{subsection:multiplierBarrelShifter}

The multiplier takes two 32-bit inputs and, after 3 clock cycles (latency),
produces a 64-bit result, but only 32 bits are output. By configuration, it is
possible to choose the upper or lower 32-bit part of the result. It is also
possible to shift the result left by 1 position, which is useful to work in the
Q1.31 fixed-point format, a very common format in many DSP algorithms.

For the barrel shifter, one operand is the word to be shifted and the other
operand is the shift size. The barrel shifter can be configured with the shift
direction (left or right) and the shift type in case of a right shift
(arithmetic or logic). The barrel shifter has a latency of 1 clock cycle.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Control and Status Register}
\label{subsection:dataEngineControl}

Inside the DE there are also two important registers used by the Controller to
manage the DE: the Control Register (write only), to initialize and start the
FUs and the Status Register (read only) to know if the DE has already finished a
specific task.

The Control Register has the following structure: bits 20 down to 2 are used for
individually selecting the 19 existents FUs (8 memory ports and 11 other FUs),
bit 1 is used to enable the selected FUs for running and bit 0 is used to apply
the initialization values to the selected FUs.

The Status Register simply reports the state of the 8 AGUs in bits 7 down to 0:
logic ’0’ indicates the AGU is running and logic ’1’ indicates the AGU is idle.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Configuration Module}
\label{section:configuration}

The DE configuration bits are, as mentioned before, organized in {\em
  configuration spaces} (each FU has its own) which are stored in the
Configuration Module (CM).  Each {\em configuration space} includes multiple
{\em configuration fields}, which are memory mapped from the Controller point of
view. The {\em configuration fields} make partial reconfiguration possible since
the Controller can change a single {\em configuration field} using a single
store instruction.  Typically, a configuration can be completed in a few clock
cycles with a small or zero time overhead: there is a configuration shadow
register which allows configuring the next datapath while the DE is running.
The CM comprises a configuration register file, a configuration shadow register,
a configuration memory and an address decoder.  The configuration register file
has 15 configuration spaces, as many as configurable FUs in Versat. Since there
are different types of FUs, the {\em configuration spaces} and the bit width of
the configuration fields differ for two different FUs.  The configuration memory
can save up to 64 configurations and is used to store the most frequently used
configurations.  Each position of the configuration memory is also memory mapped
from the Controller point of view. A read access to any such position causes the
respective contents of the configuration memory to be loaded into the
configuration register file; a write access causes the contents of the
configuration register file to be stored into the addressed position of the
configuration memory. No data is exchanged on the Control Bus during these read
or write accesses because these transactions are internal to the CM.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Controller}
\label{section:controller}

The Versat Controller is a minimal 16-instruction architecture for
reconfiguration, data transfer, and simple algorithm control.  This controller
is not meant to replace a more general host processor, which can run complex
applications while using Versat as an accelerator. On the contrary, the Versat
Controller is designed to simplify the interactions between the host processor
and Versat, freeing the host from the burden of micromanaging the accelerator.
The controller contains 3 main registers: the accumulator (RA), the address
register (RB), used for indirect loads and stores, and the program counter (PC).
The 16 instructions that the Controller can execute are presented in Table
\ref{tab:isa}.

\begin{table}[!htb]
  \centering
   \caption{Instruction set of Versat Controller.}
   \label{tab:isa}
   \begin{tabular}{|c|l|}
    \hline 
    {\bf Mnemonic} & {\bf Description} \\
    \hline \hline 
    rdw & RA = (Imm)\\
    \hline
    wrw & (Imm) = RA\\
    \hline
    rdwb & RA = (RB)\\
    \hline
    wrwb & (RB) = RA\\
    \hline
    ldi & RA = Imm\\
    \hline
    ldih & RA[31:16] = Imm\\
    \hline
    beqi & RA == 0? PC = Imm: PC += 1; RA = RA-1\\
    \hline
    beq & RA == 0? PC = (Imm): PC += 1; RA = RA-1\\
    \hline
    bneqi & RA != 0? PC = Imm: PC += 1; RA = RA-1\\
    \hline
    bneq & RA != 0? PC = (Imm): PC += 1; RA = RA-1\\
    \hline
    add & RA = RA + (Imm)\\
    \hline
    addi & RA = RA + Imm\\
    \hline
    sub & RA = RA - (Imm)\\
    \hline
    shft & RA = (Imm $<$ 0)? RA=RA$<<$1: RA=RA$>>$1\\
    \hline
    and & RA = RA \& (Imm)\\
    \hline
    xor & RA = RA \^ (Imm)\\
    \hline
     \end{tabular}
\end{table}

As usual, brackets represent memory positions. For example, (Imm) represents the
contents of the memory in address Imm. The PC is incremented for every
instruction, except for branch instructions, where it is loaded with the branch
target.
