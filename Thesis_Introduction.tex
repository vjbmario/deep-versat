%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Introduction.tex                                    %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Valter Mário                                             %
%     Last modified :  26 Dezembro 20178                               %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{Introduction}
\label{chapter:introduction}


\section{Topic Overview}
\label{section:topic}
%preamble
During the last few years we have seen big developments and investments in
technology, namely in the fields of Machine Learning (ML), Artificial
Intelligence (AI) and the Internet of Things (IoT). These developments increased
the complexity of computer algorithms and at the same time the need to lower
power consumption.

To cater for the computational demands of certain applications, it is common to
place dedicated hardware accelerators running in parallel with general purpose
processors. However, these hardware blocks are not programmable which increases
the cost of design errors as, once the circuit is fabricated, it
cannot be changed. To address this problem a programmable accelerator should be
employed such as a Graphics Processing Unit (GPU) or a Field Programmable Gate
Array (FPGA). However, these subsystems are big and power hungry and for many
applications they cannot viably be used, for instance, in many IoT applications.

% CGRA
A more suitable accelerator is the Coarse Grained Reconfigurable Array (CGRA),
which can be made small and energy efficient. A CGRA is a collection of
programmable Functional Units (FUs) and embedded memories connected by
programmable interconnects -- the Reconfigurable Array. When programmed with a
stream of configuration bits, the reconfigurable array forms hardware datapaths
able to execute certain tasks orders of magnitude faster than a conventional
Central Processing Unit (CPU). CGRAs can be used as hardware co-processors to
accelerate algorithms that are time/power consuming in regular CPUs while
offering programmable features.

Normally, the reconfigurable array is used only to accelerate program loops and
the non-loop code is run on a CPU. For this reason, CGRAs are frequently
controlled by a conventional CPU. For example, the Morphosys
architecture~\cite{Lee00} integrates a small Reduced Instruction Set Computer
(RISC) and the ADRES architecture~\cite{Mei05} integrates a Very Large
Instruction Word (VLIW) processor~\cite{lopestese}.

%Versat
This work is integrated in the Versat project, a Coarse-Grain Reconfigurable
Array (CGRA), which implements self-generated partial re-configuration using a
simple controller unit.  Unlike the existing approaches that mainly use
pre-compiled configurations, a Versat program can generate and apply myriads of
on-the-fly configurations. The space required to store the program is much
smaller than the space required to store all configurations it
generates. Partial reconfiguration plays a central role in this approach, as it
speeds up the generation of incrementally different configurations. Versat can
be effectively programmed in assembly language, which can be used to produce
optimized programs and work around post silicon hardware, software or compiler
issues. When this work started the architecture was fixed and comprised a
minimal 16-instruction controller and a full mesh Data Engine (DE). The DE
consists of 15 Functional Units (FUs) of different types including arithmetic
and logic units, multipliers, one barrel shifter, and dual-port memories.

Versat has its own controller, so it can generate its own configurations rather than
sourcing them from external memory. Self reconfiguration is the most
distinguished feature of Versat. Storing routines that generate configuration
sequences is much more efficient than storing the configurations themselves
because most configurations are similar. The similarity between configurations
is exploited by using partial reconfiguration. If only a few configuration bits
differ between two configurations, then only those bits are changed.

Versat cores are intended to be used as co-processors in an embedded system
containing one or more application processors.  These co-processors should be
deployed in the system in order to optimize performance and energy consumption
during the execution of compute intensive tasks. They can be used by application
programmers by simply calling special procedures in a program that runs on a
host processor. A Versat Application Programming Interface (API) library is
provided for that purpose.  In this way, the software and programming tools of
the CGRA are clearly separated from those of the application processor, making
the management of the heterogeneous system easier.

A single Versat can replace a number of dedicated hardware accelerators in a
System on Chip (SoC), if these different accelerators are not required to work
in parallel. This results in a smaller, more power efficient and safer component
to design with, since being programmable eliminates the development risk of
designing dedicated hardware accelerators.

\section{Motivation}
\label{section:motivation}

% the problem
Although Versat is optimized in area and relatively easy to program, it presents
a major problem: applications that could use more FUs in parallel cannot be run
because the current array is static and has a limited number of FUs. In fact,
with the work already done in the scope of this project, it is already possible
set the number of FUs of each type. However, the topology is always a full mesh
which implies a quadratic increase in the interconnect circuitry and an at least
logarithmic increase in the circuit delay caused by the selection multiplexers.
Since the number of selection inputs is a low number a logarithmic increase is
still problematic; this delay increase can be countered by the use of
pipelining at the cost of an even greater silicon area.

% the solution
Thus, the main motivation of this work is to develop an architecture that can
tackle any task, regardless of the numbers of FUs required, without affecting
the system clock frequency and with the area scaling linearly with the additional
parallel functions needed.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Objectives}
\label{section:objectives}

%only objectives please
The main objective of the dissertation is to develop a multi-layer architecture
for Versat -- the Deep Versat Architecture. In the new architecture, the number
of layers and their functional units can be configured at pre-silicon time,
depending on the envisaged application.

Each layer behaves like the current Versat implementation, in terms of
compactness and system clock frequency. Layers are stacked so that each layer
only has connections to the previous or next layer, hence limiting the overall
number of connections and preventing frequency decrease. In this way, the
interconnect circuitry grows linearly with the number of FUs needed and the
clock frequency is kept stable, allowing Versat to obtain useful acceleration at
low power consumption for any application.

In this initial part of the work the objectives are to come up with a basic
design for the Deep Versat Architecture. This implies studying the state of the
art of CGRA architectures and studying and using Versat itself, in order to
better know how it works.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Author's Work}

Some of the work presented here is also the result of a Summer Internship (2018)
and a present part time job at IObundle, the company that is sponsoring my
thesis. When I started working with Versat, there was a functional version of
this CGRA as presented in Chapter~\ref{chapter:architecture}. My main
contributions to the Versat project have happened in the context of a project
for an international client, where Versat is used to accelerate the front end of
an MP3 encoder (see Section~\ref{section:preliminarywork}). This project
demanded the implementation of some features that not only improve the
performance and functionality of Versat in the MP3 front end application but
also will allow go towards the Deep Versat Architecture objective.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Report Outline}
\label{section:outline}

This report is composed by 4 more chapters. In the second chapter, the existent
Versat architecture is fully described. In the third chapter, the current
state-of-the-art of CGRAs is studied. In the fourth chapter, a basic design of
the Versat Deep Architecture is proposed and the work already done and in progress 
are explained. Finally, in the fifth chapter, what has been achieved
so far is presented and directions for future work are outlined.
