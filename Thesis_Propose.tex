%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Propose.tex                                         %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Valter Mário                                             %
%     Last modified :  02 January 2019                                 %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{The Proposed Solution}
\label{chapter:propose}

As have been pointed out in the Introduction given in
Chapter~\ref{chapter:introduction}, the problem is directly related with
Versat's full mesh topology, which has a lot of advantages, but have a major
problem of not being scalable.  In this chapter, the conceptual idea and work in
progress to solve this problem is presented as well as its testing strategy.

% ----------------------------------------------------------------------
\section{The Concept}
\label{section:concept}
The idea is to create multiple layers in the Versat architecture, where each
layer keeps the full mesh topology in order to avoid complicated place and route
algorithms and to retain Versat's ease of use. It is impossible to scale with
full mesh between all FUs because the complexity of the interconnect circuit
will grow quadratically and the combinational circuit delay will grow at least
logarithmically.

Since Versat is already tested and works well with 15 FUs, which gives 19 output
ports (each of the 4 dual-port memories contribute 2 ports), the idea is that
each layer has also a maximum of 19 output ports. A basic design sketch of the
proposed architecture is explained in Figure~\ref{fig_de_proposed}.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{drawings/DeepVersat.png}
\caption{Data Engine proposed to Deep Versat.}
\label{fig_de_proposed}
\end{figure}

The number of FUs in each layer must be lower than 19.
It may not make sense in some applications to employ more
than one layer, but it is also limiting to have a static 1-layer architecture
as the current Versat implementation.
So the number of layers and the number and type of FUs in each layer is
parametrizable in order to make Versat be useful in general, from simple
applications like the addition of 2 vectors to compute intensive applications
like training a deep neural network.

When the application is simple and needs few FUs (less than 19) the user can
specify a lower number of FUs to optimize resources and power consumption.
For instance, if the user needs to add two vectors of 100k elements that are
placed in the same memory and save the result vector in another memory, she/he
could create an architecture with a single layer containing 2 memories and 1
ALU. The innovative part of doing this is the possibility of doing this using
RTL parameters. It is possible to do it with the current Versat
implementation but the RTL needs to be edited.

The greatest value of this work is for running compute intensive applications
that can benefit from the use of more than 19 FUs working in parallel. In those
cases it makes sense to use a multi-layered architecture. Since any application
needs memory and since memory is not exactly a functional unit, a special layer
called the Memory Layer will always exist. The constitution of the other layers
is parameterizable and in a first approach all layers are equal.

The full mesh will exist inside a layer, using an internal data bus, and also
between adjacent layers. This way the routing complexity of the Full Mesh will
be kept constant and independent of the number of FUs or layers.

The outputs of the last layer are connected to the memory layer inputs and the
memory layer outputs are connected to the first layer inputs. It is a data flow
architecture highly suitable for applications like doing inference or training
deep neural networks. Data flow architectures need no memory hierarchy, caches,
instruction fetch and other managing activities that only waste power. Stuff can
be executed in parallel, improving the number of operations per Watt.


% ----------------------------------------------------------------------
\section{Work Already Done}
\label{section:preliminarywork}

As mentioned in the Introduction in Chapter~\ref{chapter:introduction}, the
author gained extensive and valuable experience implementing the Front End (FE) of
an MP3 encoder in a real commercial project. The MP3 encoder is based in
shine~\cite{shine}, which is a blazing fast MP3 encoding library implemented in
fixed-point arithmetic.

For the MP3 FE project the control bus shown in~\ref{fig_fu} has been removed as
in practice we observed that the controller almost never needs to write or read
directly to the FUs, and there are alternative and appropriate ways to control
the FUs via the array configuration and the AGUs.

The two main contributions to the Versat architecture accomplished in this
project were the development of multiply accumulate (MAC) unit and the
introduction of pre-silicon configurability via RTL parameters. These
developments are explained in the next 2 sections.

\subsection{MULADD}

Since most compute intensive applications make use of the multiply and
accumulate operation, a new FU has been implemented in the scope of this
project that is capable of executing this operation: the MULLADD FU.

The MP3 FE implementation is derived from 2 main C code files available
in~\cite{shine}: l3mdct.c and l3subband.c. The code in these files has been
ported to Versat where the program loops run in Versat and the non-loop code
runs on the Versat controller. In file l3subband.c the following code snipped
is extracted for illustrative purposes:

\begin{lstlisting} [language=C, frame=single, label={code_ex}, caption={Example
of Dual Loop in MP3 FE.}] 
 for (i=SBLIMIT; i--; ) {
	int32_t s_value;
	uint32_t s_value_lo;
    
    mul0(s_value, s_value_lo, config->subband.fl[i][63], y[63]);
    for (j=63; j; j-=7) {
      muladd(s_value, s_value_lo, config->subband.fl[i][j-1], y[j-1]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-2], y[j-2]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-3], y[j-3]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-4], y[j-4]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-5], y[j-5]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-6], y[j-6]);
      muladd(s_value, s_value_lo, config->subband.fl[i][j-7], y[j-7]);
    }
    mulz(s_value, s_value_lo);
    s[i] = s_value;
  }
\end{lstlisting}

As can be seen, the "muladd" function, a multiply and accumulate macro, is
called multiple times. In order to implement the above loops, the MULADD FU has
been used. The MULADD FU has been implemented and tested; it has the functions
presented in Table \ref{tab_muladd}, where the notation uses A and B for the
operands and Y for result.

\begin{table}[]
\caption{MULADD Operations.}
\label{tab_muladd}
\centering
\begin{tabular}{|l|l|lll}
\cline{1-2}
MUL and DIV by 2    & Y=(A x B)\textgreater{}\textgreater{}32                                  &  &  &  \\ \cline{1-2}
MULT                & Y=(A x B)\textgreater{}\textgreater{}31                                  &  &  &  \\ \cline{1-2}
MULADD and DIV by 2 & Y = (Y + A x B)\textgreater{}\textgreater{}32                            &  &  &  \\ \cline{1-2}
MULSUB and DIV by 2 & Y = (Y - A x B)\textgreater{}\textgreater{}32                            &  &  &  \\ \cline{1-2}
MULADD              & Y = (Y + ((A x B)\textless{}\textless{}1))\textgreater{}\textgreater{}32 &  &  &  \\ \cline{1-2}
MULSUBB             & Y = (Y - ((A x B)\textless{}\textless{}1))\textgreater{}\textgreater{}32 &  &  &  \\ \cline{1-2}
MUL Low Part        & Y=(A x B){[}31:0{]}                                                      &  &  &  \\ \cline{1-2}
\end{tabular}
\end{table}

\subsection{Pre-Silicon Configurability}

Analyzing the loops in FE code, it has been concluded that only a few FUs
present in the previous static Versat architecture were needed. Hence, and
having the work that needs to be done for the Versat Deep Architecture in mind,
we implemented the automatic generation of FU arrays using Verilog parameters
and 'for generate' statements, as shown in listing~\ref{code_ver_ex}. The
listing shows the specific case for ALUs, but for the other FU types, the
process is the same.

\begin{lstlisting} [language=verilog, frame=single, label={code_ver_ex}, caption={Dynamic Generation of ALUs.}] 
 // Instantiate the ALUs
 generate
    for (i=0; i < `nALU; i=i+1) begin : add_array
 xalu alu (
      .clk(clk),
      .rst(run_reg),
      // Data IO
      .data_bus(data_bus),
      .result(data_bus[`DATA_ALU0_B - i*`DATA_W -: `DATA_W]), 
      // Configuration data
      .configdata(config_reg_shadow[`CONF_ALU0_B - i*`ALU_CONF_BITS -:
      						   `ALU_CONF_BITS])
      );
     end
 endgenerate
\end{lstlisting}

The user enters the number of FUs of each type in a header file intended for the
specific application. In Listing~\ref{code_define} it is presented the number of
FUs according to type that are used in MP3 FE program.
\newpage
\begin{lstlisting}[language=verilog, frame=single, label={code_define},
caption={Define the Numbers of FUs used in FE.}]
  // Number of functional units
  `define nMEM      4
  `define nALU      0
  `define nALULITE  1
  `define nMUL      0
  `define nMULADD   1
  `define nBS       1
\end{lstlisting}

Not just the number of FUs has been made parametrizable, but also the size of
each memory in the memory array, which is defined by the number of address bits. 

This pre-silicon configuration capability allows customizing Versat for the
target applications with large savings in area and power. The same principle
will be present in the Versat Deep Architecture.


\subsection{Example of Implementation}

The MP3 FE project is a good example of the usefulness of adjusting the size of the
Versat array. It uses just 4 memories (each one with its specific size) plus 3
FUs instead of using the 15 FUs of the previous static Versat.

Since the main effort in the MP3 FE was to write the assembly program to
configure Versat and execute the FE loops, a snippet of assembly code is
presented in Listing~\ref{code_configdp}. This code configures the datapath that
executes the loops shown in Listing~\ref{code_ex}. The code itself, with the
help of the inserted comments, is self-explanatory. This configuration results
in the datapath shown in Figure~\ref{fig_dp_resultant}.

\begin{figure}[h]
\centering
\includegraphics[width=0.4\textwidth]{drawings/DP_ex.png}
\caption{Datapath resultant from the configuration in Listing \ref{code_configdp}.}
\label{fig_dp_resultant}
\end{figure}

In Figure~\ref{fig_dp_resultant}, a multiply and accumulate operation is done
with the input data coming from memory 0, through port B, and from memory 2, through
port A. The result of the accumulation is saved, every 64 cycles, in memory 1
through port B. An important thing to mention here is the use of Port A of
memory 0 as a counter. To do that the constant 'sADDR' is written to the SEL
configuration register of the memory port, causing its output to be directly
generated by AGU which is not used in this case to generate addresses.

Of course the use of assembly code makes the process slow compared to writing
for instance in C code and have a compiler configure the datapath. The current
Versat C compiler~\cite{Santiago2016}~\cite{rec17} does not support the pre-silicon configuration
feature, only the previous static implementation. There is another work being
done in parallel with this one to implement a new and updated Versat C compiler,
which could simplify the development of applications for the Versat Deep
Architecture. In fact making datapath configurations for many layers in assembly
code will be tedious and time consuming.

\newpage
\begin{minipage}{0.45\linewidth}
\begin{lstlisting} [frame=single]
##configure mem0 and mem2 to
##compute final value of subband
 ldi 2367 # 2304 + 63
 wrc CONF_MEM0B,MEMP_CONF_START
 ldi 2047
 wrc CONF_MEM2A,MEMP_CONF_START
 ldi 32
 wrc CONF_MEM0B,MEMP_CONF_ITER
 wrc CONF_MEM2A,MEMP_CONF_ITER
 ldi 64
 wrc CONF_MEM0B,MEMP_CONF_PER
 wrc CONF_MEM2A,MEMP_CONF_PER
 wrc CONF_MEM0B,MEMP_CONF_SHIFT
 wrc CONF_MEM0B,MEMP_CONF_DUTY
 wrc CONF_MEM2A,MEMP_CONF_DUTY
 ldi -1
 wrc CONF_MEM0B,MEMP_CONF_INCR
 wrc CONF_MEM2A,MEMP_CONF_INCR

##configure AGU to counter 
##from 0 to 64, to control MULADD
 ldi sADDR
 wrc CONF_MEM0A,MEMP_CONF_SEL
 ldi 0
 wrc CONF_MEM0A,MEMP_CONF_START
 ldi 32
 wrc CONF_MEM0A,MEMP_CONF_ITER
 ldi 1
 wrc CONF_MEM0A,MEMP_CONF_INCR
 wrc CONF_MEM0A,MEMP_CONF_DELAY
 ldi 64
 wrc CONF_MEM0A,MEMP_CONF_PER
 wrc CONF_MEM0A,MEMP_CONF_DUTY
 ldi -64
 wrc CONF_MEM0A,MEMP_CONF_SHIFT
\end{lstlisting}
\end{minipage}%
\hspace{0.1\linewidth}
\begin{minipage}{0.45\linewidth}
\begin{lstlisting}[frame=single]
##MUL_ADS
 ldi sMEM0B
 wrc CONF_MULADD0,MULADD_CONF_SELA
 ldi sMEM2A
 wrc CONF_MULADD0,MULADD_CONF_SELB
 ldi sMEM0A
 wrc CONF_MULADD0,MULADD_CONF_SELO
 ldi MULADD_MACC_DIV2
 wrc CONF_MULADD0,MULADD_CONF_FNS
 ldi sMULADD0
 wrc CONF_MEM1B,MEMP_CONF_SEL

##The value of Muladd are saved in
##Mem1B each 63 iterations
 rdw R6
 wrc CONF_MEM1B,MEMP_CONF_START
 ldi 32
 wrc CONF_MEM1B,MEMP_CONF_ITER
 ldi 64
 wrc CONF_MEM1B,MEMP_CONF_PER
 wrc CONF_MEM1B,MEMP_CONF_DUTY
 ldi 4
 wrc CONF_MEM1B,MEMP_CONF_DELAY
 ldi -1
 wrc CONF_MEM1B,MEMP_CONF_SHIFT

##run engine
 ldi 1
 wrw ENG_RUN_REG
 nop
 nop

##wait for computation finish
wait3 rdw ENG_RDY_REG
      beqi wait3
\end{lstlisting}
\end{minipage}
\begingroup
\captionof{lstlisting}{Assembly Code to Configure
Datapath to Execute the loops presented in Listing \ref{code_ex}}\label{code_configdp}
\endgroup


% ----------------------------------------------------------------------
\section{Preliminary Results and Expected Results}
\label{section:expectedResults}

In order to evaluate the functionality and advantage of the Versat Deep
Architecture, two applications will be tested. One is the described MP3 encoder
front end and the other will be decided in the future and will for sure need
multiple layers. A good candidate is the Convolutional Neural Network Versat
application to be developed in the scope of another master's thesis to be
started in 2019's Spring Semester. For this report no quantitative results on
the new architecture can yet be presented as it is currently under
development. However, some qualitative results extracted from the MP3 FE
experience can be described. These are summarized in the next section, titled
{\em Application with few FUs}. In section~\ref{section:ica}, titled {\em
  Compute Intensive Application}, we extrapolate the qualitative evaluation to
the case of the multi-layer Versat architecture.

\subsection{Application with few FUs}
\label{section:aff}

An example with few FUs is the aforementioned MP3 FE application. Besides the
details already explained, chiefly the writing the Versat assembly program for
the complete MP3 FE , in this section an explanation of its simulation process
is given, which was also a major achievement in this work.

A compute intensive simulation process was put in the place to validate the
design before synthesis and implementation in FPGA. The simulation environment
made use of Cadence's NCSIM simulator and can be explained by the diagram
presented in Figure~\ref{fig_sim_proc}.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.55\textwidth]{drawings/Simulation_Diagram.png}
\caption{Simulation Process.}
\label{fig_sim_proc}
\end{figure}

The PC is used as the host embedded processor and NCSim is used as the Versat
hardware accelerator. The compute intensive FE part runs on Versat while the
rest of the MP3 encoding algorithm runs on the host processor.

Since it is an MP3 encoder application, the process is repetitive for each audio
frame extracted from a .wav audio file. After reading a frame (normally 1152
audio samples) from the .wav file, the frame is written to the Versat core by
emulating a DMA transfer. This is accomplished by writing the frame contents to
a file named 'data\_in.hex'. This is the input that Versat needs to do the
computation along with some encoding parameters. The host then performs a system
call to start the NCSIM simulator.

When the simulation starts, the testbench uses the 'data\_in.hex' to load the
Versat memories and start it. When the simulation finishes, the test bench
writes the result data to a file named 'data\_out.hex'. This file is then read
by the software application to check if the results match, bit by bit, with the
ones calculated in the software. This process is called bit true testing.

Thanks to this simulation process, many bugs were detected and fixed, and when
the system was finally synthesized and implemented in the FPGA it worked at the
first try.

Since the project is still in progress the evaluation in terms of execution
time, area and frequency has not been completed but the general idea is that
Versat can accelerate the MP3 FE algorithm by one order of magnitude compared to
the Intel NIOS2 processor.


\subsection{Compute Intensive Application}
\label{section:ica}

For a compute intensive application, since the multi-layer architecture is not
already implemented, is not possible to present any performance results.

First of all an application need be selected, most likely a deep neural network
application that is being started in another master's thesis on the Versat CGRA.
The Versat Deep Architecture is organized in layers with full mesh connections
between adjacent layers resembling the topology of the deep neural networks
themselves, where each layer takes the outputs of the previous layer and
produces the inputs for the next layer.

All this points to significant performance improvements when using Versat for
executing deep neural network algorithms. Hence, speedups and power savings of
multiple orders of magnitude can be expected compared to regular CPUs. Then, this
application needs to be run in other acceleration platforms and compared to
Versat. The performance gains may not be significant or may not even
exist. However, advantages in frequency of operation, energy consumption and
silicon area are expected since Versat is one of the few high performance
architectures that targets low cost low power devices.


% ----------------------------------------------------------------------



% ----------------------------------------------------------------------

%\section{Comparing with other CGRAs}
%\label{subsection:OtherCGRAs}

%It is difficult to compare the Versat performance with the other
%CGRAs. Published results always omit important information, and it is
%hard to ascertain the conditions under which they have been
%obtained. A possible solution would be to replicate the other
%approaches in order to make fair comparisons. However, CGRAs are
%complex cores and implementing them from their descriptions in
%research papers, besides representing a formidable effort, is not
%guaranteed to yield trustworthy results, as some important details
%could be missed.

%Despite ADRES being one of the most cited CGRA architectures,
%comparisons with this architecture could not be made, since the
%execution times for the examples used in its publications have not
%been given in absolute terms.


