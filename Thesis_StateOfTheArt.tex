%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                      %
%     File: Thesis_Background.tex                                      %
%     Tex Master: Thesis.tex                                           %
%                                                                      %
%     Author: Valter Mário                                             %
%     Last modified :  28 December 2018                                %
%                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\chapter{CGRA State-of-the-Art}
\label{chapter:background}

A good review on the state-of-the-art of CGRAs can be found in~\cite{25years}
and served as a basis for the present review.

In the last 25 years, reconfigurable architectures, namely Coarse-Grained
Reconfigurable Architectures (CGRAs), have become popular since they can adapt
the architecture to a specific application and still provide acceleration. There
is interest both in academia~\cite{Mei05}~\cite{Flex} and also in
industry~\cite{WAVE}~\cite{PACT}.

The work done in the reconfigurable architectures domain is not only about
hardware design, but also about tools that enable people with less hardware
knowledge to make use of these systems. For instance, the OpenCL support for
FPGAs~\cite{opencl} helps engineers generate high-performance acceleration
hardware without much hardware know-how. On the other hand, with this type of
tools, the FPGAs are used at much coarser granularity compared to designing
using hardware description languages (HDL), such as verilog and VHDL. These may
indicate that FPGAs are moving in the direction of CGRAs.

Since the main objective of this work is to upgrade the Versat architecture,
most of the work presented here is on architecture.

According to~\cite{25years}, a CGRA architecture must have the following properties: 
\begin{itemize}
\item Spatial reconfiguration granularity at the functional unit (FU) level or above.
\item Temporal reconfiguration granularity at the region/loop-nest level or above. 
\end{itemize}

Based on this definition, Figure~\ref{fig_cgra_pos} shows where CGRAs are in terms of
spacial/temporal granularity and resource type compared with other architectures.

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{drawings/CGRA_pos.png}
	\caption{Spatial-temporal architecture landscape.~\cite{25years}}
	\label{fig_cgra_pos}
\end{figure}

Usually, in CGRA studies, there are four main aspects that are evaluated:
structure, (re)configuration, integration and tooling, discussed in the next
sections.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Structure}

The structure of an architecture is mainly related with interconnect topology,
functional unit granularity and heterogeneity versus homogeneity. Direct
neighbour-to-neighbour connections such as~\cite{REMARC} or 2D-Mesh networks
such as~\cite{FPCA} are the most popular choices~\cite{25years}. As said before,
Versat uses a full mesh topology, but this type of topology has been avoided as
they scale poorly in terms of area, wire delays and power consumption. However,
for small arrays the simplicity and programmabilty of Versat beats all other
architectures and its disadvantages do not surface. This is clear from the MP3
application explained in section~\ref{chapter:propose}.

Notwithstanding, with the advent of machine learning algorithms large arrays are
often needed, so it is important to keep a lean interconnection structure which
provides the basic motivation to extend Versat to a multi-layer architecture,
which is the basic theme in this work.

As far as granularity is concerned, the majority of the CGRA architectures
operate on a word-level granularity but some are multi-granular, which
allows the adaptation of the datapath to the application at a lower
level~\cite{25years}.

Some CGRAs are homogeneous~\cite{Ebeling96}, i.e., they only have one type of
functional unit, whereas others are heterogeneous and support a diversity of
functional units~\cite{Heysters03}. A careful analysis done in~\cite{Park12} has
favored heterogeneous CGRAs, arguing that the performance degradation when going
from a homogeneous to a heterogeneous architecture is greatly compensated by the
better silicon area utilization and power efficiency of heterogeneous
solutions~\cite{lopestese}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Reconfiguration}
\label{section:control}

The reconfiguration can be dynamic or static and full or partial. These two
reconfiguration characteristics will be discussed in the next sections.

\subsection{Dynamic vs Static}

Static reconfiguration is when the array is configured once to run a
complete kernel~\cite{Hartenstein01}. This is a straight forward approach but
has poor flexibility.

Some arrays are dynamically reconfigurable but they only iterate over a fixed
sequence of configurations~\cite{Lee00}~\cite{Mei05}. These configurations must be
moved to the CGRA from an external memory and stored inside the CGRA, which
costs memory bandwidth and internal storage space. Following a fixed sequence of
configurations means that it is impossible to conditionally choose the next
configuration. Furthermore, in many cases, it is the host system that manages
the reconfiguration~\cite{Lee00}~\cite{Mei05}. This is inefficient because the host
could be doing more useful tasks. It also makes programming and integrating
CGRAs in an embedded system difficult~\cite{lopestese}.

\subsection{Full vs Partial}

Oftentimes, the operations done in two different kernels are similar and they
use almost the same FUs. Some architectures, such as DRRA~\cite{DRRA},
RaPiD~\cite{Ebeling96} and PACT~\cite{PACT}, use this fact to improve the
reconfiguration performance by means of partial reconfiguration, while most CGRAs
are only fully reconfigurable~\cite{Lee00}~\cite{Mei05}. The advantage of partial
reconfiguration is the few amount of configuration data that must be kept and/or
fetched from external memory.

The DRRA~\cite{DRRA} approach supports partial configuration because the
configuration bits are distributed and controlled by independent controllers.
RaPiD supports dynamic (cycle by cycle) partial reconfiguration for a subset of
the configuration bitstream, which implies that the loop body may take several
cycles to execute. The partial reconfiguration process in PACT is called
differential configuration, but it is rarely used as they claim to have an
efficient technique to do full reconfiguration~\cite{PACT}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Integration}

The integration of a CGRA is related with its function in a system. Mainly it
can be used as an accelerator~\cite{Lee00}\cite{Mei05}, being tightly or loosely
coupled to a host processor, or support stand-alone operations~\cite{Trips}, not
interacting with an host processor at all. Versat is supposed to be used as an
accelerator and interacts with the host processor through a slave interface for
commands and a master interface for data, internally connected to a DMA unit.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Tooling}

The success of any architecture depends heavily on the available tool
support~\cite{25years}.

The tools that support CGRAs include an assembler, a compiler, place and route
tools and, eventually, a design space exploration (DSE) tool. Versat does not
need a place and route tool because only a basic place and route is needed due
to the full mesh structure.

With partial reconfiguration, the mapping and routing are done at run
time. There are different types of compilers, often they are based in the C
language like the one that is being developed in parallel in another master's
thesis. Most of the existing architectures do not provide any DSE.

There are also a parallel works being developed in other master's theses that
consists in development of a C compiler and simulator for Versat, and therefore
this state-of-the-art review is not covering those subjects.

